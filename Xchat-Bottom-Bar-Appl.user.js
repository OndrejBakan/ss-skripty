﻿// ==UserScript==
// @Originalname Xchat-Bottom-Bar-Appl.user.js
// @Author gooryk
// @include http://*xchat.centrum.cz/*$11992435*/modchat?op=textpageng*
// @include http://*xchat.centrum.cz/*$11992435*/modchat
// @include http://*xchat.centrum.cz/*$9431549*/modchat?op=textpageng*
// @include http://*xchat.centrum.cz/*$9431549*/modchat
// @include http://*xchat.centrum.cz/*$4799162*/modchat?op=textpageng*
// @include http://*xchat.centrum.cz/*$4799162*/modchat

// @include http://*xchat.centrum.cz/*$12320923*/modchat?op=textpageng*
// @include http://*xchat.centrum.cz/*$12320923*/modchat
// @include http://*xchat.centrum.cz/*$12331631*/modchat?op=textpageng*
// @include http://*xchat.centrum.cz/*$12331631*/modchat
// ==/UserScript==
//**Pokud pouzivate prvky vedle odesilaciho tlacitka: zatrhavaci policko + boxik pro sept, tak zalomitkujte // nasledujici dva radky**
document.getElementsByName("aret")[0].style.display="none";						//jeden z moznych zapisu	//Zatrhavaci policko
document.getElementsByName("target")[0].setAttribute("style","display:none");	//druhy z moznych zapisu	//Boxik s nicky pro septani

//***************************************GLOBALNI PROMENNE**************************************************************************
var myshort = ":o) ";			//Zde napsat mezi uvozovky, co chci vedle radku pro psani misto meho nicku napr var myshort = "Bůh";

//****ORIGINALNI ziskani dat z kodu hlavni stranky..zarucuje ze bude existovat vzdy a tim konecne odstranim plno problemu - nyni novejsi verze osetreni proti zmenam nyni hleda v celem HTML kodu****					
var data = top.window.document.getElementsByTagName("html")[0].innerHTML;	//nemenit!
var cid = data.split("var cid=")[1].split(";")[0];							//nemenit!

if (cid==8 || cid==32){							//tento radek nemenit!
//POZOR pouze kat. SEX - Zde lze menit nazvy a zneni duvodu ve vybiracim boxiku pro duvody - mente pouze text mezi uvozovkama!
//Nazvy						//Duvody	
n1="Porušování pravidel";	d1="Porušování pravidel Xchatu nebo místnosti!";
n2="Kluk";					d2="Nenavštěvuj místnosti pouze pro holky, když jsi kluk!";
n3="Reklama";				d3="Neroznášej reklamní, anketní odkazy a ani odkazy na zvýšení návštěvnosti!";
n4="Tel.číslo";				d4="Telefonní číslo ani jiné osobní údaje na sklo nepatří!";
n5="Tapetace";				d5="Tapetace-nezahlcuj sklo smajlama ani opakujícím se, či nadměrným textem v krátké době!";
n6="UEFA";					d6="Bezdůvodné vyhazování!";
n7="Kredit";				d7="Nenabízej ani nežádej kredit za cokoli!";
n8="Foto &lt 18";			d8="Nehledej ani nenabízej er. materiál nebo focení er. rázu osob nebo osobám mladším 18, natož 15let!";  //max. delka co se vleze do duvodu
n9="Pedo";					d9="Nehledej, nenabízej a ani se nebav o sexu jak s osobami, tak i o osobách mladších 15let!";
n10="Incest";				d10="Nehledej, nenabízej a ani se nebav o incestu!";
n11="Animal";				d11="Nehledej, nenabízej a ani se nebav o animal sexu!";
n12="SzP";					d12="Nenabízej ani nehledej sex, er. služby za peníze, dárky, ubytování či jiné odměny, bez vydržování!";
n13="&lt 15";				d13="Nenavštěvuj místnosti se sexem, když je ti pod 15let!";
n14="&lt 18";				d14="Nenavštěvuj místnosti s věkovou hranicí 18let a více!";
n15="Za kick infobota";		d15="Nevyhazuj informačního robota místnosti, pokud ti vadí, dej si ho do ignorace!";
n16="Nevhodné chování";		d16="Nevhodné chování na Xchatu!";
n17="Narušování chodu";		d17="Nenarušuj zaběhlý chod místnosti!";
}									//tento radek nemenit!
else{								//tento radek nemenit!
//MIMO SEX mistnosti - Zde lze menit nazvy a zneni duvodu ve vybiracim boxiku pro duvody - mente pouze text mezi uvozovkama!
//Nazvy						//Duvody	
n1="Porušování pravidel";	d1="Porušování pravidel Xchatu nebo místnosti!";
n2="Reklama";				d2="Neroznášej reklamní, anketní odkazy a ani odkazy na zvýšení návštěvnosti!";
n3="SEX";					d3="Nehledej, nenabízej a ani se zde nebav o sexu. K tomu jsou určeny jiné místnosti!";
n4="Tel.číslo";				d4="Telefonní číslo ani jiné osobní údaje na sklo nepatří!";
n5="Tapetace";				d5="Tapetace-nezahlcuj sklo smajlama ani opakujícím se, či nadměrným textem v krátké době!";
n6="UEFA";					d6="Bezdůvodné vyhazování!";
n7="Kredit";				d7="Nenabízej ani nežádej kredit za cokoli!";
n8="Za kick infobota";		d8="Nevyhazuj informačního robota místnosti, pokud ti vadí, dej si ho do ignorace!";
n9="Nevhodné chování";		d9="Nevhodné chování na Xchatu!";
n10="Narušování chodu";		d10="Nenarušuj zaběhlý chod místnosti!";
}									//tento radek nemenit!
//********************************Tohle nemenit!!!*********************************
var userframe = top.window.frames[5];
var input = document.getElementById("msg");									//Promenna - radek pro psani
var send_button = document.getElementsByName("submit_text").item(0);		//Promenna - odesilaci tlacitko
var nick_text = document.getElementsByTagName('strong')[0];					//Promenna - kam se vlozi tlacitka
//**********************************************************************************************************************************

//**************************************************Zde se vytvari tlacitka*********************************************************
if (cid==8 || cid==32) nick_text.innerHTML = "<table><tr><td><input type='button' id='info' value='I' title='Info, kde se nick nachází.' style='cursor:pointer;font-style:obligue;font-weight:bold;font-size:xx-small;border:solid #1E3A39;color:black;background-color: yellow;width: 20px'>"+" <input type='button' id='info2' value='I2' title='Podrobnější info o nicku.' style='cursor:pointer;font-style:obligue;font-weight:bold;font-size:xx-small;border:solid #1E3A39 ;color:black;background-color: yellow;width: 20px'>"+" <input type='button' id='key' value='Key' title='Přidá nick do klíčů.' style='cursor:pointer;font-style:obligue;font-weight:bold;font-size:xx-small;border:solid #1E3A39 ;color:black;background-color: yellow;width: 30px'>"+" <input type='button' id='unkey' value='Uky' title='Odebere nick z klíčů.' style='cursor:pointer;font-style:obligue;font-weight:bold;font-size:xx-small;border:solid #1E3A39 ;color:black;background-color: yellow;width: 30px'>"+" <input type='button' id='admin' value='A' title='Předá správce nicku nebo bez zvolení nicku dojde k automatickému předání správce.' style='cursor:pointer;font-style:obligue;font-weight:bold;font-size:xx-small;border:solid #1E3A39 ;color:blue;background-color: white;width: 20px'>"+" <input type='button' id='unkick' value='U' title='Unkick nicku.' style='cursor:pointer;font-style:obligue;font-size:xx-small;font-weight:bold;border:solid #1E3A39 ;color:white;background-color: green;width: 20px'> <span style='position:relative;top:18px'>"+myshort+":</span></td></tr><tr><td><select id='reason' title='Výběr důvodů pro kick.' style='font-style:obligue;font-size:xx-small;font-weight:bold;border:solid #CD0000;color:black;background-color: yellow;width:63px'><option value='"+d1+"'>"+n1+"</option><option value='"+d2+"'>"+n2+"</option><option value='"+d3+"'>"+n3+"</option><option value='"+d4+"'>"+n4+"</option><option value='"+d5+"'>"+n5+"</option><option value='"+d6+"'>"+n6+"</option><option value='"+d7+"'>"+n7+"</option><option value='"+d8+"'>"+n8+"</option><option value='"+d9+"'>"+n9+"</option><option value='"+d10+"'>"+n10+"</option><option value='"+d11+"'>"+n11+"</option><option value='"+d12+"'>"+n12+"</option><option value='"+d13+"'>"+n13+"</option><option value='"+d14+"'>"+n14+"</option><option value='"+d15+"'>"+n15+"</option><option value='"+d16+"'>"+n16+"</option><option value='"+d17+"'>"+n17+"</option></select>" + " <input type='text' id='reasonplace' value='' title='Sem vložte vlastní znění důvodu pro kick.' style='font-style:obligue;font-size:x-small;border:solid #CD0000;color:white;background-color: black;width: 60px'>"+" <input type='button' id='kick' value='K' title='Kick nicku!' style='cursor:pointer;font-style:obligue;font-size:xx-small;font-weight:bold;border:solid #CD0000;color:black;background-color: yellow;width: 20px'>"+"</td></tr></table>";
else nick_text.innerHTML = "<table><tr><td><input type='button' id='info' value='I' title='Info, kde se nick nachází.' style='cursor:pointer;font-style:obligue;font-weight:bold;font-size:xx-small;border:solid #1E3A39;color:black;background-color: yellow;width: 20px'>"+" <input type='button' id='info2' value='I2' title='Podrobnější info o nicku.' style='cursor:pointer;font-style:obligue;font-weight:bold;font-size:xx-small;border:solid #1E3A39 ;color:black;background-color: yellow;width: 20px'>"+" <input type='button' id='key' value='Key' title='Přidá nick do klíčů.' style='cursor:pointer;font-style:obligue;font-weight:bold;font-size:xx-small;border:solid #1E3A39 ;color:black;background-color: yellow;width: 30px'>"+" <input type='button' id='unkey' value='Uky' title='Odebere nick z klíčů.' style='cursor:pointer;font-style:obligue;font-weight:bold;font-size:xx-small;border:solid #1E3A39 ;color:black;background-color: yellow;width: 30px'>"+" <input type='button' id='admin' value='A' title='Předá správce nicku nebo bez zvolení nicku dojde k automatickému předání správce.' style='cursor:pointer;font-style:obligue;font-weight:bold;font-size:xx-small;border:solid #1E3A39 ;color:blue;background-color: white;width: 20px'>"+" <input type='button' id='unkick' value='U' title='Unkick nicku.' style='cursor:pointer;font-style:obligue;font-size:xx-small;font-weight:bold;border:solid #1E3A39 ;color:white;background-color: green;width: 20px'> <span style='position:relative;top:18px'>"+myshort+":</span></td></tr><tr><td><select id='reason' title='Výběr důvodů pro kick.' style='font-style:obligue;font-size:xx-small;font-weight:bold;border:solid #CD0000;color:black;background-color: yellow;width:63px'><option value='"+d1+"'>"+n1+"</option><option value='"+d2+"'>"+n2+"</option><option value='"+d3+"'>"+n3+"</option><option value='"+d4+"'>"+n4+"</option><option value='"+d5+"'>"+n5+"</option><option value='"+d6+"'>"+n6+"</option><option value='"+d7+"'>"+n7+"</option><option value='"+d8+"'>"+n8+"</option><option value='"+d9+"'>"+n9+"</option><option value='"+d10+"'>"+n10+"</option></select>" + " <input type='text' id='reasonplace' value='' title='Sem vložte vlastní znění důvodu pro kick.' style='font-style:obligue;font-size:x-small;border:solid #CD0000;color:white;background-color: black;width: 60px'>"+" <input type='button' id='kick' value='K' title='Kick nicku!' style='cursor:pointer;font-style:obligue;font-size:xx-small;font-weight:bold;border:solid #CD0000;color:black;background-color: yellow;width: 20px'>"+"</td></tr></table>";
//**************Zde prirazeni funkci k talcitkum dle udalosti***********************
document.getElementById("info").addEventListener("click", info, false);
document.getElementById("info2").addEventListener("click", info2, false);
document.getElementById("kick").addEventListener("click", kick, false);
document.getElementById("unkick").addEventListener("click", unkick, false);
document.getElementById("key").addEventListener("click", key, false);
document.getElementById("unkey").addEventListener("click", unkey, false);
document.getElementById("admin").addEventListener("click", admin, false);
//**********************************************************************************************************************************

//***************************************** Tyto promenne musi byt zde pod vytvorenim tlacitek !************************************
var blockuzivduvod = document.getElementById("reasonplace"); 			//promenna - hodnoty z: spodni obdelnik pro kill duvody
var duvodkill= document.getElementById("reason");						//promenna - vybiraci boxik duvodu pro kill
//**********************************************************************************************************************************

//*********************************************************POMOCNE FUNKCE***********************************************************
function stripchars(correct){											//Fce co odstrani nepovolene znaky v nicku, vychazi z pravidel pro registraci nicku!//EDIT: Jelikoz sem obejvil nekolik nicku co maj vice tecek a i na konci musel sem zmenit kod...ale do budoucna necham zalomitkovany kod presne dle reg. pravidel
	correct = correct.replace(/[^a-zA-Z0-9\._-]+/g,"");					//- neobsahuje české znaky - obsahuje pouze znaky a-z, A-Z, 0-9, . (tečka), _ (podtržítko), - (pomlčka)
	correct = correct.replace(/^[\._-]+/g,"");
	return correct
}

function puttext(what){												// vpise text na radek nebo pro refresh radku + odesle
	input.value = what;
	if(what !="") send_button.click();
    input.focus();
}

function zdroj(p){ 		//Zjistuje, jestli je text (nick,nazev, vyraz,..)  brany ze  skla nebo z radku//Lze nastavit prioritu ci prvni ze skla nebo z radku zamenou poradi,// parametr p pokud je roven 0, pak se vypne odstraneni nevhodnych znaku
	var glass = top.window.frames[1].frames[0];
	nick = "";
	if(input.value != "") nick = p==0 ? input.value : stripchars(input.value);
	else if(glass.document.getSelection() != "") nick = p==0 ? glass.document.getSelection() : stripchars(glass.document.getSelection());
	return nick;
}
//*********************************************************************************************************************************

//**********************************************Funkce pro tlacitka****************************************************************
function admin()puttext("/admin "+zdroj());

function unkick(){
	if(zdroj() != "") puttext("/unkick "+zdroj());
	else puttext("");
}

function kick(){
	var glass = top.window.frames[1].frames[0];
	var inforamecek=glass.document.getElementById("inforamecek");
	var infodiv=glass.document.getElementById("infodiv");
	var refresh=glass.document.getElementById("refresh");
	var sipka=glass.document.getElementById("sipka");
	blockreason = blockuzivduvod.value == "" ? duvodkill.value : blockuzivduvod.value;
	if(zdroj() != ""){
		infodiv.style.display="block";sipka.style.display="none";
		inforamecek.innerHTML="<i>Nick <b id='nick'>"+zdroj()+"</b> by měl být vykopnut s důvodem: <b id='duvod'>"+blockreason+"</b></i>";
		inforamecek.style.color='blue';refresh.select();
		puttext("/kick "+zdroj()+" "+blockreason);
	}
	else puttext("");
}

function key(){
	if(zdroj() != "")puttext("/key "+zdroj());
	else puttext("");
}

function unkey(){
	if(zdroj() != "")puttext("/unkey "+zdroj());
	else puttext("");
}

function info2(){
	var useriframe = userframe.document.getElementById("xuser");			//musi byt zde protoze se meni stranka kde ten prvek je a pak nefunguje
	if(zdroj() != ""){
		if (useriframe.src.match(/http:\/\/xchat\.beedoo\.net\/admin\/Support/g)!= null) useriframe.src="http://xchat.beedoo.net/admin/Support/x.php?user="+zdroj();
		puttext("/info2 "+zdroj());
   	}
   	else puttext("");
}

function info(){
	if(zdroj() != "")puttext("/info "+zdroj());
	else puttext("");
}
//*********************************************************************************************************************************

//************************************INFO RADEK O KICKU A DUVODU VE SKLE**********************************************************
top.window.addEventListener("load", function () {proved()}, false);			//fce volana az po nacteni cele stranky

function proved(){
	var kam = top.window.frames[1].frames[0].document.body;
	var kod="var infodiv=document.getElementById('infodiv');var nick=document.getElementById('nick').innerHTML;var duvod=document.getElementById('duvod').innerHTML;var input=top.window.frames[3].document.getElementById('msg');var send_button = top.window.frames[3].document.getElementsByName('submit_text').item(0);input.value='*3871* Nick \'+nick+\' byl vykopnut z důvodu: \'+duvod+\' *3871*';send_button.click();input.focus();infodiv.style.display='none';";
	var kodrefr="var refresh=document.getElementById('refresh');var zmena=Math.random()*1000;refresh.value=zmena/*Kvuli zajisteni refreshu tzn. pro vypnuti vsech timeintervalu pri jednom ci vicenasobnem kliknuti na kick*/;var cyklu=527;var cas=4;var r=0;var g=0;var b=255;var interval=window.setInterval(time,38);var interval2=window.setInterval(odpocetf,1000);var odpocet=document.getElementById('odpocet');odpocet.innerHTML='*';function odpocetf(){if(refresh.value==zmena){cas--;if(cas<1){clearInterval(interval2);odpocet.innerHTML='';document.getElementById('sipka').style.display='inline';}else odpocet.innerHTML+='*';}else clearInterval(interval2);};function time(){if(refresh.value==zmena){cyklu--;if(b>194)b--;else if(r<244)r++;else if(g<222)g++;if(cyklu<1){clearInterval(interval);document.getElementById('infodiv').style.display='none';}else document.getElementById('inforamecek').style.color='rgb(\'+r+\',\'+g+\',\'+b+\')';}else clearInterval(interval);};"
	kam.innerHTML="<div id='infodiv' style='position:relative;top:-10px;left:-10px;font-size:1.25em;background-color:#F4DEC2;border: 1px solid blue;color:black;padding-left:10px;padding-bottom:5px;padding-right:40px;display:none'><img src='http://xteam.jaw.cz/gooryk/admin-scripty/data/info.png' style='position:relative;height:25px;width:25px;top:5px'>&nbsp;<b>Info:</b>&nbsp;<span id='inforamecek' style='color:blue'></span><span style='position:absolute;top:10px;left:100%;margin-left:-35px;color:red;font-size:18px;font-weight:bold'><span id='odpocet'></span><img id='sipka' title='Kliknutím se odešle na sklo oznáméní o kicku nicku se zněním důvodu.' src='http://xteam.jaw.cz/gooryk/admin-scripty/data/Down_Arrow.png' style='position:relative;cursor:pointer;height:25px;width:25px;top:-5px' onclick=\""+kod+"\"></span><input type='text' id='refresh' value='' style='display:none;width:0px' onselect=\""+kodrefr+"\"</div>"+kam.innerHTML;
}