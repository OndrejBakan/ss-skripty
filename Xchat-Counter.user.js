﻿// ==UserScript==
// @Originalname Xchat-Counter.user.js
// @Author gooryk
// @include http://*xchat.centrum.cz/*/modchat
// @include http://*xchat.centrum.cz/*/modchat?op=textpageng*
// @exclude http://*xchat.centrum.cz/~guest~/modchat*
// ==/UserScript==

var send_button = document.getElementsByName("submit_text").item(0);
var input = document.getElementById("msg");

send_button.title='Odesílací tlačítko, zobrazuje počet zbývajících znaků, které lze vepsat. Rozšířený mód: 170znaků až 200 - zvýraznění zbývajících 30znaků do stand. 200znaků; 201 - 499znak - zvýraznění rozšířeného módu; 500 a více - zšednutí, nelze již vkládat žádné další znaky.';
input.setAttribute("maxlength","500");

function update_counter(){
	send_button.value = 500 - input.value.length;
	if (input.value.length > 170 && input.value.length < 201){
		input.style.color="red";
		input.style.background="#BBDDF7";
	}
	else if (input.value.length > 200 && input.value.length < 500){
		input.style.color="white";
		input.style.background="red";
	}	
	else if (input.value.length > 499){
		input.style.color="grey";
		input.style.background="red";
		send_button.value = 0;
	}	
	else {										//Reset barev
		input.style.color="";
		input.style.background="";
	}
}

input.addEventListener("keyup",update_counter,false);
input.addEventListener("focus",update_counter,false);