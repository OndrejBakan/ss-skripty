﻿// ==UserScript==
// @Originalname Xchat-Whispercontroller.user.js
// @Author gooryk + MedaBeedaa
// @include http://*xchat.centrum.cz/*/modchat?op=roomtopng*
// @exclude http://*xchat.centrum.cz/~guest~/modchat*
// ==/UserScript==

//***************************************GLOBALNI PROMENNE**************************************************************************
//**************Volitelne promenne********************
var dobeeps = false; 					// Kdyz chcem pipani u septani, prepsat false na true
var barva = "blue";						// Mezi uvozovky napsat jakou barvou ma byt text v septacich tlacitkach, lze pouzit i RGB HTML kod

var ripadress = "84.16.224.52"
var rport = "10794"
//*****************************************************
var data = top.window.document.getElementsByTagName("html")[0].innerHTML;		//nacteni zdroje dat
var mynick = data.split("var my_nick='")[1].split("';")[0];
var textframe = top.window.frames[3];
var infoframe = top.window.frames[2];
var userframe = top.window.frames[5];
var place = infoframe.document.body.getElementsByTagName('strong');
var space = "";
var butt,buttons,idsout,valueout,timesout,glassupdate,lines,outbuttonline,time,timesec,timesecx;
var nbuttons = 0;
var j,num,i,position,output,ressearch;
var glass,useriframe,soundiframe,stavp,tubkoplayer,infosongokno;

idsout=new Array();
valueout=new Array();
timesout=new Array();
//**********************************************************************************************************************************

//**********************************************Ochrana proti chybam vlivem refresh a zpozdeni**************************************
var nacitani;
if (top.window.document.readyState!="complete") nacitani=window.setInterval(check,100);		//test jestli bylo zmacknuto F5
else nactipromenne();																		//pro ultra okamzite nacteni pro pripady kdy nejde o F5

function check(){							//Unikatni funkce co testuje (vyhleda) presnycas zobrazeni (nacteni) skla i s textem
	if (top.window.frames[1].frames[0].document.readyState=="complete"){
		if (top.window.frames[1].frames[0].document.body.innerHTML.match(/<div id="board"><div id="splash">Nahrávám\.\.\. <img src=/g)==null){
			clearInterval(nacitani);
			nactipromenne();
		}
	}
}

function nactipromenne(){														//Nacteni problemyovych promennych az kdyz existuji
	glass = top.window.frames[1].frames[0];
	useriframe = userframe.document.getElementById("xuser");
	soundiframe = userframe.document.getElementById("sounder");
	stavp = infoframe.document.getElementById('stavp');							//promenna existence prepinace pro prehravac
	tubkoplayer = glass.document.getElementById('prehraj');						//promenna pro youtube prehravac
	infosongokno = top.window.frames[10].document.getElementById('infosong');	//promenna pro infosongokno
	start();
}
//**********************************************************************************************************************************

//**********************************************Program pro septaci obdelniky*******************************************************
function start(){

function searchifexists(nick){
	var i,out;
	out = -1;
	for(i = 0;i<nbuttons;i++){
		if(idsout[i] == nick) {
			out = i;
			break;
		}
	}
	return out;
}

function setbutton(nick){
	butt = infoframe.document.getElementById(nick);
	butt.addEventListener("click", function () {
		textframe.document.forms[0].elements[5].value = '/m ' + this.id + ' ';
		textframe.document.forms[0].elements[5].focus();
	}, false);
}

if(place.length == 4){
	place[0].innerHTML = "</strong><b style=\"font-size:medium\">=&gt; </b><strong>"+"</strong><br/><strong>"+place[0].innerHTML;
}
else{
	space = place[1].innerHTML;
	buttons = infoframe.document.getElementsByTagName('input');
	for(j=0;j<buttons.length;j++){
		idsout[j] = buttons[j].id;
		num = buttons[j].value.split("\n");
		num = num[1].split(" | ");
		valueout[j] = parseInt(num[0]);
		timesout[j] = num[1];
		nbuttons++;
	}
}

try {
	glassupdate = this.document.body.innerHTML;

	lines = glassupdate.split("<font ");

	time = new Date();
	timesec = 3600*time.getHours() + 60 *time.getMinutes() + time.getSeconds();
	
	for(j=lines.length-1;j>0;j--){
		output = lines[j].split("</font>");
		position = output[0].toLowerCase().search("->"+mynick);
		
		if(position > 0){
			output = output[0].split("</span></a>->");
			if(output.length > 1) {
				output = output[0].split(">");
				output = output[output.length-1].toLowerCase();
			}
			else continue;

			if(dobeeps == true) soundiframe.setAttribute("src", "http://xchat.beedoo.net/admin/Support/sidebarvoice.php?user=" + output);
			
			ressearch = searchifexists(output);
			if (ressearch == -1){
				idsout[nbuttons] = output;
				valueout[nbuttons] = 1;
				timesout[nbuttons] = time.getHours()+":"+time.getMinutes();
				nbuttons++;
				if (useriframe.src.match(/http:\/\/xchat\.beedoo\.net\/admin\/Support/g)!= null) useriframe.setAttribute("src", "http://xchat.beedoo.net/admin/Support/x.php?user="+output);		//toto zde je proto, aby se mi nemazaly infoobdelniky, kdyz tam je neco jineho napr ¨zapisy na fora nebo kontorla zak. vstupu
			}
			else{
				valueout[ressearch] += 1;
			}
		}

		output = lines[j].split("</font>");
		position = output[0].toLowerCase().search(mynick+"->");
		
		if(position > 0){
			output = output[0].split("</span></a>:</b>");
			if(output.length > 1) {
				output = output[0].split(">");
				output = output[output.length-1].toLowerCase();
			}
			else continue;

			ressearch = searchifexists(output);
			if (ressearch == -1){
				;
			}
			else{
				valueout[ressearch] = 0;
				timesout[ressearch] = time.getHours()+":"+time.getMinutes();
			}
		}
	}

	outbuttonline = "";
	for(j=0;j< nbuttons;j++){
		timesecx = timesout[j].split(":");
		timesecx = 3600*parseInt(timesecx[0])+60*parseInt(timesecx[1]);
		if((timesec - timesecx) > 600) {
			idsout[j] = "";
			continue;
		}
		if(valueout[j] == 0){
			outbuttonline = outbuttonline + "<input type=\"button\" id=\""+idsout[j]+"\" value=\""+idsout[j]+"\n"+valueout[j]+" | "+timesout[j]+"\" style=\"font-style:obligue;font-weight:bold;font-size:xx-small;color:"+barva+";border:dashed green thick;background-color: transparent;\"> ";
		}
		else{
			outbuttonline = outbuttonline + "<input type=\"button\" id=\""+idsout[j]+"\" value=\""+idsout[j]+"\n"+valueout[j]+" | "+timesout[j]+"\" style=\"font-style:obligue;font-weight:bold;font-size:xx-small;color:"+barva+";border:solid red thick;background-color: transparent;\"> ";
		}
	}
	place[1].innerHTML = outbuttonline;
	for(j=0;j< nbuttons;j++){
		if(idsout[j] == "") continue;
		else setbutton(idsout[j]);
	}
	
}
catch(exception) {
        ;
}
//****************************************VYHLEDANI KICKU A ZMENY SPRAVCE***********************************************************
var radky=glass.document.getElementById("board").getElementsByTagName("div");
for (a=0;a<radky.length;a++){
	if (radky[a].innerHTML.match(/<font class="systemtext" size="-2">/g)){				//aby to neoznacovalo, kdyz to nekdo zkopiruje , ciste jen oznacuje systemovky
		if (radky[a].innerHTML.match(/byl.*? vyhozen.*?správcem/g)!=null)radky[a].setAttribute("style","color:white;background-color:#CC0000");
		else if (radky[a].innerHTML.match(/Správcem se stává/g)!=null)radky[a].setAttribute("style","color:white;background-color:#009933");
		else if (radky[a].innerHTML.match(/předal.*? správcovství/g)!=null)radky[a].setAttribute("style","color:white;background-color:#0099CC");
	}
}
//**********************************************************************************************************************************

//*************************************************VLOZENI YOUTUBE TLACITEK DO SKLA*************************************************
var sklo=glass.document.getElementById("board");
var radky=sklo.getElementsByTagName("div");
var idbutton=0;

if (tubkoplayer!=null){									//Vložení tlačítek s adresama pro youtube prehravac
	for (a=radky.length-1;a>-1;a--){					//radky prohledavat odspodu kvuli aktualizaci ID ..ovsem musi jit text odshora dolu!
		nalezen=radky[a].innerHTML.match(/http:\/\/www\.youtube\.com\/watch\?v.+?&|http:\/\/www\.youtube\.com\/watch\?v.+?<\/span>.*?=.+? |http:\/\/www\.youtube\.com\/watch\?v.+?<\/span>.*?=.+?<\/span>|http:\/\/youtu\.be\/.+?<\/a>|http:\/\/youtu\.be\/.+? |http:\/\/youtu\.be\/.+?<\/span>/g);
		nalezentl=radky[a].innerHTML.match(/Ybutton/g);
		skok=nalezentl!=null ? nalezentl.length : 0;							//pocet nalezenych tlacitek v radku - promenna slouzi pro navyseni ciselneho oznaceni tlacitek..kdyz dojde k objeveni noveho odkazu na skle
		if (nalezen!=null && nalezentl==null){
			var kodyvidea=new Array();
			for (b=nalezen.length-1;b>-1;b--){									//od zadu protoze jinak by bylo obracene poradi tlacitek vzhledem k odkazum na skle
				idbutton++
				kodvidea=nalezen[b].replace(/http:\/\/www\.youtube\.com\/watch\?v.+?<\/span>.*?=|http:\/\/youtu\.be\/|<\/a>|&|<\/span>| /g,"");
				kodvidea="Button"+idbutton+" "+kodvidea;
				kodyvidea.push(kodvidea);
				radky[a].innerHTML="<input name='Ybutton' id='"+kodvidea+"' type='button' style='font-style:obligue;font-size:10px;background-color: black;font-weight:bold;height:30px;width:30px;cursor:pointer;color:#00FF00' title='Otevři v Youtubeoknu - levý klik-starší verze / pravý - nejnovější verze přehrávače' value='♫'>"+radky[a].innerHTML;
			}
			for (b=0;b<kodyvidea.length;b++){									//prirazovani udalosti musi byt zde, protoze kdyz je vice odkazu v jednom radku, zmeni se radek pokazdem vlozeni tlacitka a pak prestanou fungovat jiz prirazene udalosti, proto se to musi priradit az po uplnem dokonceni zmen v radku!				
				var playtl = glass.document.getElementById(kodyvidea[b]);
				playtl.addEventListener("click", function () {vlozURL(this.id,false)}, false);
				playtl.addEventListener("contextmenu", function () {vlozURL(this.id,true)}, false);
			}
		}
		else if (nalezentl!=null) idbutton+=skok;
	}
}
else{												//odstraneni tlacitek pro youtube prehravac
	tlacitka=glass.document.getElementsByName('Ybutton');
	if (tlacitka.length!=0){
		delka=tlacitka.length;						//meni se tlacitka length..s kazdym cyklem ..musi tu byt toto misto a<tlacitka.length
		for (a=0;a<delka;a++)tlacitka[0].parentNode.removeChild(tlacitka[0]);			//!!!Jelikoz se neustale odebira prvni(nulta) polozka ..musi byt vzdy bran nulty prvek!!!
	}
}

function vlozURL(kod,rclick){						//fce prirazeni URL pro jednotliva tlacitka, po stisku levym na tl. se vlozi tato URL do youtube prehravace a vybere starsi prehravac (dle mne lepsi), pravym novejsi
	kod=kod.replace(/Button.+? /g,"");
	if (rclick) URL="http://www.youtube.com/v/"+kod+"?version=3&autoplay=1";
	else URL="http://www.youtube.com/v/"+kod+"?version=2&autoplay=1";
	tubkoplayer.src=URL;
	top.window.frames[3].document.getElementById("msg").focus();
}
//****************************REFRESH INFOOKNA songu pro radio*******************************
//refresh okynka pro nazvy songu .. je to diky teto strance co se obnovuje jako se obnovuje sklo
if (stavp!=null && infosongokno!=null) infosongokno.src = "http://"+ripadress+"/chat/fr2.php?ip="+ripadress+"&port="+rport+"&b=1&radioid=738506";
//**********************************************************************************************************************************
}